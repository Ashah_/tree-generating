// source code: https://ciphered.xyz/2019/09/11/generating-a-3d-growing-tree-using-a-space-colonization-algorithm/

using System.Collections.Generic;
using UnityEngine;

public class MeshCreator : MonoBehaviour
{
    [Header("Mesh generation")]
    [Range(0, 20)]
    public int radialSubdivisions = 10;
    [Range(0f, 1f), Tooltip("The size at the extremity of the branches")]
    public float extremitiesSize = 0.05f;
    [Range(0f, 5f), Tooltip("Growth power, of the branches size")]
    public float invertGrowth = 2f;
    
    public void ToMesh(List<Branch> branches, MeshFilter meshFilter)
    {
        Mesh treeMesh = new Mesh();

        ComputeBranchSize(branches);

        Vector3[] vertices = new Vector3[(branches.Count + 1) * radialSubdivisions];
        int[] triangles = new int[branches.Count * radialSubdivisions * 6];

        // construction of the vertices 
        for (int i = 0; i < branches.Count; i++)
        {
            Branch branch = branches[i];

            // the index position of the vertices
            int verticesId = radialSubdivisions * i;
            branch.verticesId = verticesId;

            // quaternion to rotate the vertices along the branch direction
            Quaternion quaternion = Quaternion.FromToRotation(Vector3.up, branch.direction);

            // construction of the vertices 
            for (int subdivision = 0; subdivision < radialSubdivisions; subdivision++)
            {
                // radial angle of the vertex
                float angularStep = (float)subdivision / radialSubdivisions * Mathf.PI * 2f; // krok kątowy

                // radius is hard-coded to 0.1f for now
                Vector3 pos = new Vector3(Mathf.Cos(angularStep) * branch.size, 0, Mathf.Sin(angularStep) * branch.size);
                pos = quaternion * pos; // rotation

                // if the branch is an extremity, we have it growing slowly
                if (branch.children.Count == 0 && !branch.grown)
                {
                    pos += branch.start + (branch.end - branch.start);
                }
                else
                {
                    pos += branch.end;
                }

                vertices[verticesId + subdivision] = pos - transform.position; // from tree object coordinates to [0; 0; 0]

                // if this is the tree root, vertices of the base are added at the end of the array 
                if (branch.parent == null)
                {
                    vertices[branches.Count * radialSubdivisions + subdivision] = branch.start + new Vector3(Mathf.Cos(angularStep) * branch.size, 0, Mathf.Sin(angularStep) * branch.size) - transform.position;
                }
            }
        }

        // faces construction; this is done in another loop because we need the parent vertices to be computed
        for (int i = 0; i < branches.Count; i++)
        {
            Branch branch = branches[i];
            int fid = i * radialSubdivisions * 2 * 3;
            // index of the bottom vertices 
            int bId = branch.parent != null ? branch.parent.verticesId : branches.Count * radialSubdivisions;
            // index of the top vertices 
            int tId = branch.verticesId;

            // construction of the faces triangles
            for (int s = 0; s < radialSubdivisions; s++)
            {
                // the triangles 
                triangles[fid + s * 6] = bId + s;
                triangles[fid + s * 6 + 1] = tId + s;

                if (s == radialSubdivisions - 1)
                {
                    triangles[fid + s * 6 + 2] = tId;
                }
                else
                {
                    triangles[fid + s * 6 + 2] = tId + s + 1;
                }

                if (s == radialSubdivisions - 1)
                {
                    // if last subdivision
                    triangles[fid + s * 6 + 3] = bId + s;
                    triangles[fid + s * 6 + 4] = tId;
                    triangles[fid + s * 6 + 5] = bId;
                }
                else
                {
                    triangles[fid + s * 6 + 3] = bId + s;
                    triangles[fid + s * 6 + 4] = tId + s + 1;
                    triangles[fid + s * 6 + 5] = bId + s + 1;
                }
            }
        }

        treeMesh.vertices = vertices;
        treeMesh.triangles = triangles;
        treeMesh.RecalculateNormals();

        meshFilter.mesh = treeMesh;
    }

    private void ComputeBranchSize(List<Branch> branches)
    {
        for (int i = branches.Count - 1; i >= 0; i--)
        {
            float size = 0f;
            Branch branch = branches[i];

            if (branch.children.Count == 0)
            {
                size = extremitiesSize;
            }
            else
            {
                foreach (Branch branchChild in branch.children)
                {
                    size += Mathf.Pow(branchChild.size, invertGrowth);
                }

                size = Mathf.Pow(size, 1f / invertGrowth);
            }

            branch.size = size;
        }
    }
}
