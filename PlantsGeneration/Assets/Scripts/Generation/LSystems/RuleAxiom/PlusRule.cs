using System.Collections.Generic;
using Generation.LSystems;
using Generation.LSystems.RuleAxiom;
using UnityEngine;

[CreateAssetMenu(fileName = "PlusRule", menuName = "LSystem/Rules/PlusRule")]
public class PlusRule : LSystemRule
{
    #region Serialized Fields

    [SerializeField]
    private Vector2 foldAngle = new(10, 20);

    #endregion

    #region Public Methods

    public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
    {
        transform.Rotate(Vector3.back * Random.Range(foldAngle.x, foldAngle.y));
        return null;
    }

    #endregion
}