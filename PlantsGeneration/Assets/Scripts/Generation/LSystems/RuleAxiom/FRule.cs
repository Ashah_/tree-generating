using System.Collections.Generic;
using Generation.LSystems.RuleAxiom;
using UnityEngine;

[CreateAssetMenu(fileName = "FRule", menuName = "LSystem/Rules/FRule")]
public class FRule : LSystemRule
{
    #region Serialized Fields

    [SerializeField]
    private float length = 2.0f;

    #endregion

    #region Public Properties

    public List<Branch> Branches { get; set; } = new List<Branch>();

    #endregion

    #region Public Methods

    public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
    {
        initialPosition = transform.position;
        transform.Translate(Vector3.up * length);

        Vector3 startPos = initialPosition;
        Vector3 direct = Vector3.up;

        direct = transform.rotation * direct;
        direct.Normalize();

        direct += startPos;
        Vector3 endPos = direct;

        Branch meshBranch = null;

        if (lastBranch != null)
        {
            meshBranch = new Branch(startPos, endPos, direct.normalized, lastBranch);
            lastBranch.children.Add(meshBranch);
        }
        else
        {
            meshBranch = new Branch(startPos, endPos, direct.normalized);
        }

        Branches.Add(meshBranch);

        return meshBranch;
    }

    #endregion
}