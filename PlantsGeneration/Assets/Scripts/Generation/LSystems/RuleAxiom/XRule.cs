using UnityEngine;

namespace Generation.LSystems.RuleAxiom
{
    [CreateAssetMenu(fileName = "XRule", menuName = "LSystem/Rules/XRule")]
    public class XRule : LSystemRule
    {
    }
}