using System.Collections.Generic;
using UnityEngine;

namespace Generation.LSystems.RuleAxiom
{
    [CreateAssetMenu(fileName = "StarRule", menuName = "LSystem/Rules/StarRule")]
    public class StarRule : LSystemRule
    {
        #region Serialized Fields

        [SerializeField]
        private Vector2 foldAngle = new(10, 20);

        #endregion

        #region Public Methods

        public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
        {
            transform.Rotate(Vector3.up * Random.Range(foldAngle.x, foldAngle.y));
            return null;
        }

        #endregion
    }
}