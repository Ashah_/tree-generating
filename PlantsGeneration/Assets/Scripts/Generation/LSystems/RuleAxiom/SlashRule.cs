using System.Collections.Generic;
using UnityEngine;

namespace Generation.LSystems.RuleAxiom
{
    [CreateAssetMenu(fileName = "SlashRule", menuName = "LSystem/Rules/SlashRule")]
    public class SlashRule : LSystemRule
    {
        #region Serialized Fields

        [SerializeField]
        private Vector2 foldAngle = new(10, 20);

        #endregion

        #region Public Methods

        public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
        {
            transform.Rotate(Vector3.down * Random.Range(foldAngle.x, foldAngle.y));
            return null;
        }

        #endregion
    }
}