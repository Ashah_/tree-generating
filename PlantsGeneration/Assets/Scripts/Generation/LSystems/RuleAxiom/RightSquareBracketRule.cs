using System.Collections.Generic;
using Generation.LSystems;
using Generation.LSystems.RuleAxiom;
using UnityEngine;

[CreateAssetMenu(fileName = "RightSquareBracketRule", menuName = "LSystem/Rules/RightSquareBracketRule")]
public class RightSquareBracketRule : LSystemRule
{
    #region Public Methods

    public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
    {
        TransformInfo ti = transformStack.Pop();
        transform.position = ti.position;
        transform.rotation = ti.rotation;

        return ti.branch;
    }

    #endregion
}