using System.Collections.Generic;
using UnityEngine;

namespace Generation.LSystems.RuleAxiom
{
    [CreateAssetMenu(fileName = "LeftSquareBracketRule", menuName = "LSystem/Rules/LeftSquareBracketRule")]
    public class LeftSquareBracketRule : LSystemRule
    {
        #region Public Methods

        public override Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
        {
            transformStack.Push(new TransformInfo
            {
                position = transform.position,
                rotation = transform.rotation,
                branch = lastBranch
            });
        
            return null;
        }

        #endregion
    }
}