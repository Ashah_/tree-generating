using System.Collections.Generic;
using UnityEngine;

namespace Generation.LSystems.RuleAxiom
{
    public class LSystemRule : ScriptableObject
    {
        #region Public Properties

        [field: SerializeField]
        public char Axiom { get; set; }
        [field: SerializeField]
        public string Rule { get; set; }

        #endregion

        #region Public Methods

        public virtual Branch Generate(ref Vector3 initialPosition, Transform transform, Branch lastBranch, Stack<TransformInfo> transformStack)
        {
            return null;
        }

        #endregion
    }
}