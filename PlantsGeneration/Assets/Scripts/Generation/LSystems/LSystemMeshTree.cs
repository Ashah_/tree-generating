using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Generation.LSystems
{
    public class LSystemMeshTree : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Rules rules;
        [SerializeField]
        private int maxDepth = 4;
        [SerializeField]
        private string treeDrawString;
        [SerializeField]
        private int treeCount = 1;

        [SerializeField]
        private MeshCreator meshCreator;

        #endregion

        #region Private Fields

        private Stack<TransformInfo> transformStack = new Stack<TransformInfo>();
        private Vector3 initialPosition = Vector3.zero;
        private List<Branch> branches = new List<Branch>();

        #endregion

        #region Unity Callbacks

        private void OnValidate()
        {
            rules.OnValidate();
        }

        private void Start()
        {
            Generate();
        }

        #endregion

        #region Private Methods

        //----------generacja  drzew-------------
        private void Generate()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int j = 0; j < treeCount; j++)
            {
                branches = new List<Branch>();
                (rules['F'] as FRule)?.Branches.Clear();
                
                string rulesString = "";
                string treeDrawStringTemp = treeDrawString;
                
                GameObject go = new GameObject();
                MeshFilter mf = go.AddComponent<MeshFilter>();
                go.AddComponent<MeshRenderer>();

                for (int i = 0; i < maxDepth; i++)
                {
                    foreach (char ruleAxiom in treeDrawStringTemp)
                    {
                        rulesString += rules.ContainAxiom(ruleAxiom) && !string.IsNullOrEmpty(rules[ruleAxiom].Rule) ? rules[ruleAxiom].Rule : ruleAxiom.ToString();
                    }

                    treeDrawStringTemp = rulesString;
                    rulesString = "";
                }

                Branch lastBranch = null;

                foreach (char axiom in treeDrawStringTemp)
                {
                    var newBranch = rules[axiom].Generate(ref initialPosition, transform, lastBranch, transformStack);
                    lastBranch = newBranch ?? lastBranch;
                }

                branches = (rules['F'] as FRule)?.Branches;
                meshCreator.ToMesh(branches, mf);

                Debug.Log("liczna wierzcholkow: " + mf.mesh.vertexCount);
                
                transformStack = new Stack<TransformInfo>();
                initialPosition = Vector3.zero;
            }

            stopwatch.Stop();

            TimeSpan elapsedTime = stopwatch.Elapsed;
            Debug.Log(elapsedTime.TotalMilliseconds + " ms");
        }

        #endregion

        //----------rysowanie kształtu z punktów-------------
        private void OnDrawGizmos()
        {
            foreach (Branch branch in branches)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(branch.start, branch.end);
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(branch.end, 0.1f);
                Gizmos.DrawSphere(branch.start, 0.1f);
            }
        }
    }
}