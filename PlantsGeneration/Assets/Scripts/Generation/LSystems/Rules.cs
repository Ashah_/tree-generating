using System;
using System.Collections.Generic;
using System.Linq;
using Generation.LSystems.RuleAxiom;
using UnityEngine;

namespace Generation.LSystems
{
    [Serializable]
    public class Rules
    {
        #region Serialized Fields

        [SerializeField]
        public List<LSystemRule> rulesList;

        #endregion

        public LSystemRule this[char ruleIndex] { get => rulesList.First(x => x.Axiom == ruleIndex); }

        #region Unity Callbacks

        public void OnValidate()
        {
            foreach (LSystemRule firstElement in rulesList)
            {
                foreach (LSystemRule secondElement in rulesList)
                {
                    if (firstElement == secondElement)
                    {
                        continue;
                    }

                    if (firstElement.Axiom.Equals(secondElement.Axiom))
                    {
                        Debug.LogError("List has same axioms!");

                        return;
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        public bool ContainAxiom(char axiom)
        {
            return rulesList.Exists(x => x.Axiom.Equals(axiom));
        }

        #endregion
    }
}