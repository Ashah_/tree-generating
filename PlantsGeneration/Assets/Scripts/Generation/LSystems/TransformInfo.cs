using UnityEngine;

public class TransformInfo
{
    public Branch branch;
    public Vector3 position;
    public Quaternion rotation;
}