using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Branch
{
    public Branch parent;
    public List<Branch> children = new List<Branch>();
    
    public Vector3 start;
    public Vector3 end;
    public Vector3 direction;
    
    public int verticesId; // the index of the vertices within the vertices array 
    public bool grown;
    public float size;
    
    // space
    public int distanceFromRoot;
    public List<Vector3> leaves = new List<Vector3>();
    
    // fractal
    public float rotationX = 0f;
    public float rotationY = 0f;
    public float rotationZ = 0f;

    #region Constructors

    public Branch(Vector3 start, Vector3 end, Vector3 direction, Branch parent = null)
    {
        this.start = start;
        this.end = end;
        this.direction = direction;
        this.parent = parent;
    }

    #endregion
}
