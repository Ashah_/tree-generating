using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Generation.Fractal
{
    public class FractalMeshTree : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Vector2Int brunchDivide;
        [SerializeField]
        private int maxDepth;
        [SerializeField]
        private Vector2 foldAngleX;
        [SerializeField]
        private Vector2 foldAngleZ;
        [SerializeField, Min(1)]
        private int branchLength = 1;
        [SerializeField]
        private float deviationX = 30f;
        [SerializeField]
        private float deviationY = 30f;
        [SerializeField]
        private float deviationZ = 30f;
        [SerializeField]
        private int treeCount = 1;

        [SerializeField]
        private MeshCreator meshCreator;

        #endregion

        #region Private Fields

        private List<Branch> branches = new List<Branch>();

        #endregion

        #region Unity Callbacks

        //----------generacja serii drzew-------------
        private void Start()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < treeCount; i++)
            {
                branches = new List<Branch>();
                
                Branch branch = new Branch(Vector3.zero, Vector3.up, Vector3.up);
                branches.Add(branch);
                DrawBrunch(1, branch, 1);

                GameObject go = new GameObject();
                MeshFilter mf = go.AddComponent<MeshFilter>();
                go.AddComponent<MeshRenderer>();

                meshCreator.ToMesh(branches, mf);
            }

            stopwatch.Stop();
            TimeSpan elapsedTime = stopwatch.Elapsed;
            Debug.Log(elapsedTime.TotalMilliseconds + " ms");
        }

        #endregion

        #region Private Methods

        private void DrawBrunch(int depth, Branch lastBranch, int currentBranchLength)
        {
            if (depth >= maxDepth * branchLength)
            {
                lastBranch.grown = false;

                return;
            }

            int brunchDivideRandom = currentBranchLength == branchLength ? Random.Range(brunchDivide.x, brunchDivide.y) : 1;
            float yAngle = Mathf.Clamp(360f / brunchDivideRandom, lastBranch.rotationY - deviationY, lastBranch.rotationY + deviationX);

            for (float i = 0; i < brunchDivideRandom; i++)
            {
                float foldAngleRandomX = Mathf.Clamp(Random.Range(foldAngleX.x, foldAngleX.y), lastBranch.rotationX - deviationX, lastBranch.rotationX + deviationX);
                float foldAngleRandomZ = Mathf.Clamp(Random.Range(foldAngleZ.x, foldAngleZ.y), lastBranch.rotationZ - deviationZ, lastBranch.rotationZ + deviationZ);

                Vector3 startPos = lastBranch.end;
                Vector3 direc = Vector3.up;

                Quaternion rotationX = Quaternion.AngleAxis(foldAngleRandomX, Vector3.right);
                Quaternion rotationZ = Quaternion.AngleAxis(foldAngleRandomZ, Vector3.forward);
                Quaternion rotationY = Quaternion.AngleAxis(yAngle * i, Vector3.up);

                direc = rotationX * direc;
                direc = rotationZ * direc;
                direc = rotationY * direc;

                direc.Normalize();

                direc += startPos;
                Vector3 endPos = direc;

                Branch branch = new Branch(startPos, endPos, direc.normalized, lastBranch);
                branch.rotationX = foldAngleRandomX;
                branch.rotationY = yAngle * i;
                branch.rotationZ = foldAngleRandomZ;

                lastBranch.children.Add(branch);

                branches.Add(branch);
                DrawBrunch(depth + 1, branch, currentBranchLength == branchLength ? 1 : currentBranchLength + 1);
            }
        }

        #endregion

        //----------rysowanie kształtu z punktów-------------
        private void OnDrawGizmos()
        {
            foreach (Branch branch in branches) 
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(branch.start, branch.end);
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(branch.end, 0.1f);
                Gizmos.DrawSphere(branch.start, 0.1f);
            }
        }
    }
}