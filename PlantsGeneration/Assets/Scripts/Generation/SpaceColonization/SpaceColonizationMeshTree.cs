// source code: https://ciphered.xyz/2019/09/11/generating-a-3d-growing-tree-using-a-space-colonization-algorithm/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Generation.SpaceColonization
{
    public class SpaceColonizationMeshTree : MonoBehaviour
    {
        #region Serialized Fields

        [Header("Generation parameters"), SerializeField, Range(0f, 0.5f)]
        private float branchLength = 0.2f;
        [SerializeField, Range(0f, 2f)]
        private float killRange = 0.5f;
        [SerializeField, Range(0f, 3f)]
        private float leaveRange = 0.1f;
        [SerializeField, Range(0, 3000)]
        private int leavesCount = 400;
        [SerializeField, Range(0f, 0.2f)]
        private float randomGrowth = 0.1f;
        [SerializeField, Range(0f, 20f)]
        private float sphereRadius = 5f;
        [SerializeField]
        private Vector3 startPosition = new Vector3(0, -7, 0);
        [SerializeField]
        private int treeCount = 1;

        [SerializeField]
        private MeshCreator meshCreator;

        #endregion

        #region Private Fields

        private List<Branch> branches = new List<Branch>();
        private List<int> activeLeaves = new List<int>();
        private List<Branch> extremities = new List<Branch>();
        private List<Vector3> leaves = new List<Vector3>();

        #endregion

        #region Unity Callbacks

        //----------generacja serii drzew-------------
        private void Start()
        {
            int leavesCountTemp = leavesCount;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < treeCount; i++)
            {
                leavesCount = leavesCountTemp;
                branches = new List<Branch>();
                activeLeaves = new List<int>();
                extremities = new List<Branch>();
                leaves = new List<Vector3>();
                
                GenerateLeavesInSphere();

                Branch firstBranch = new Branch(startPosition, startPosition + new Vector3(0, branchLength, 0), Vector3.up);
                branches.Add(firstBranch);
                extremities.Add(firstBranch);

                Generate();

                GameObject go = new GameObject();
                MeshFilter mf = go.AddComponent<MeshFilter>();
                go.AddComponent<MeshRenderer>();

                meshCreator.ToMesh(branches, mf);
            }

            stopwatch.Stop();

            TimeSpan elapsedTime = stopwatch.Elapsed;
            Debug.Log(elapsedTime.TotalMilliseconds + " ms");
        }

        #endregion

        #region Private Methods

        private void Generate()
        {
            while (true)
            {
                foreach (Branch b in extremities)
                {
                    b.grown = true;
                }

                for (int i = leaves.Count - 1; i >= 0; i--)
                {
                    foreach (Branch b in branches)
                    {
                        if (Vector3.Distance(b.end, leaves[i]) < killRange)
                        {
                            leaves.Remove(leaves[i]);
                            leavesCount--;

                            break;
                        }
                    }
                }

                if (leaves.Count <= 0)
                {
                    break;
                }

                activeLeaves.Clear();

                foreach (Branch b in branches)
                {
                    b.leaves.Clear();
                }

                int leaveIndex = 0;

                foreach (Vector3 leave in leaves)
                {
                    float min = float.MaxValue;
                    Branch closest = null;

                    foreach (Branch b in branches)
                    {
                        float distance = Vector3.Distance(b.end, leave);

                        if (distance < leaveRange && distance < min)
                        {
                            min = distance;
                            closest = b;
                        }
                    }

                    if (closest != null)
                    {
                        closest.leaves.Add(leave);
                        activeLeaves.Add(leaveIndex);
                    }

                    leaveIndex++;
                }

                if (activeLeaves.Count != 0)
                {
                    extremities.Clear();

                    List<Branch> newBranches = new List<Branch>();

                    foreach (Branch branch in branches)
                    {
                        if (branch.leaves.Count > 0)
                        {
                            Vector3 direction = new Vector3(0, 0, 0);

                            foreach (Vector3 branchLeave in branch.leaves)
                            {
                                direction += (branchLeave - branch.end).normalized;
                            }

                            direction /= branch.leaves.Count;
                            direction += RandomGrowthVector();
                            direction.Normalize();

                            Branch newBranch = new Branch(branch.end, branch.end + direction * branchLength, direction, branch);
                            newBranch.distanceFromRoot = branch.distanceFromRoot + 1;
                            branch.children.Add(newBranch);
                            newBranches.Add(newBranch);
                            extremities.Add(newBranch);
                        }
                        else
                        {
                            if (branch.children.Count == 0)
                            {
                                extremities.Add(branch);
                            }
                        }
                    }

                    branches.AddRange(newBranches);
                }
                else
                {
                    for (int i = 0; i < extremities.Count; i++)
                    {
                        Branch extremity = extremities[i];
                        Vector3 start = extremity.end;
                        Vector3 direction = extremity.direction + RandomGrowthVector();
                        Vector3 end = extremity.end + direction * branchLength;
                        Branch newBranch = new Branch(start, end, direction, extremity);

                        extremity.children.Add(newBranch);

                        branches.Add(newBranch);
                        extremities[i] = newBranch;
                    }
                }
            }
        }

        private Vector3 RandomGrowthVector()
        {
            float alpha = Random.Range(0f, Mathf.PI);
            float theta = Random.Range(0f, Mathf.PI * 2f);

            Vector3 pt = new Vector3(Mathf.Cos(theta) * Mathf.Sin(alpha), Mathf.Sin(theta) * Mathf.Sin(alpha), Mathf.Cos(alpha));

            return pt * randomGrowth;
        }

        private void GenerateLeavesInSphere()
        {
            for (int i = 0; i < leavesCount; i++)
            {
                float radius = Random.Range(0f, 1f);
                radius = Mathf.Pow(Mathf.Sin(radius * Mathf.PI / 2f), 0.8f);
                radius *= sphereRadius;
                float alpha = Random.Range(0f, Mathf.PI);
                float theta = Random.Range(0f, Mathf.PI * 2f);

                Vector3 pt = new Vector3(radius * Mathf.Cos(theta) * Mathf.Sin(alpha), radius * Mathf.Sin(theta) * Mathf.Sin(alpha), radius * Mathf.Cos(alpha));

                pt += transform.position;

                leaves.Add(pt);
            }
        }

        #endregion

        //----------rysowanie kształtu z punktów-------------
        private void OnDrawGizmos()
        {
            foreach (Branch branch in branches)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(branch.start, branch.end);
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(branch.end, 0.1f);
                Gizmos.DrawSphere(branch.start, 0.1f);
            }
        
            // foreach (Vector3 leave in leaves)
            // {
            //     Gizmos.color = Color.blue;
            //     Gizmos.DrawSphere(leave, 0.05f);
            // }
        }
    }
}