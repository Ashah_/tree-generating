using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Vector3 MulXZ(this Vector3 vector, float val)
    {
        vector = new Vector3(vector.x * val, vector.y, vector.z * val);
        return vector;
    }

    public static T GetRandomElement<T>(this List<T> list)
    {
        var index = Random.Range(0, list.Count - 1);
        return list[index];
    }
}