using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tools
{
    [Serializable]
    public class SerializedDictionary<T, K>
    {
        #region Serialized Fields

        [SerializeField]
        public List<DictionaryElement<T, K>> dictionary;

        #endregion

        public K this[T key] { get => dictionary.First(x => x.key.Equals(key)).value; }

        #region Unity Callbacks

        public void OnValidate()
        {
            foreach (DictionaryElement<T, K> firstElement in dictionary)
            {
                foreach (DictionaryElement<T, K> secondElement in dictionary)
                {
                    if (firstElement == secondElement)
                    {
                        continue;
                    }

                    if (firstElement.key.Equals(secondElement.key))
                    {
                        Debug.LogError("Dictionary has same keys!");

                        return;
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        public bool ContainKey(T key)
        {
            return dictionary.Exists(x => x.key.Equals(key));
        }

        #endregion
    }

    [Serializable]
    public class DictionaryElement<T, K>
    {
        #region Serialized Fields

        [SerializeField]
        public T key;
        [SerializeField]
        public K value;

        #endregion
    }
}